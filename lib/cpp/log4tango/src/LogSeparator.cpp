/*
 * LogSeparator.cpp
 *
 * See the COPYING file for the terms of usage and distribution.
 */
 
#include <log4tango/LogSeparator.hh>

namespace log4tango {

  LogInitiator LogInitiator::_begin_log;

  LogSeparator LogSeparator::_end_log;

} // namespace log4tango
