/*
 * tango_rolling_file_appender.h
 *
 * by NL - SOLEIL - 09/2002.
 *
 * $Revision: 3.0 $
 *
 * $Log: tangorollingfileappender.h,v $
 * Revision 3.0  2003/03/25 16:48:04  taurel
 * Many changes for Tango release 3.0 including
 * - Added full logging features
 * - Added asynchronous calls
 * - Host name of clients now stored in black-box
 * - Three serialization model in DS
 * - Fix miscellaneous bugs
 * - Ported to gcc 3.2
 * - Added ApiUtil::cleanup() and destructor methods
 * - Some internal cleanups
 * - Change the way how TangoMonitor class is implemented. It's a recursive
 *   mutex
 *
 * Revision 2.2  2003/03/11 17:55:58  nleclercq
 * Switch from log4cpp to log4tango
 *
 * Revision 2.1  2003/02/17 14:57:45  taurel
 * Added the new Tango logging stuff (Thanks Nicolas from Soleil)
 *
 *
 */

#ifndef _LOG4TANGO_TANGO_ROLLING_FILE_APPENDER_H_
#define _LOG4TANGO_TANGO_ROLLING_FILE_APPENDER_H_

#ifdef TANGO_HAS_LOG4TANGO

namespace Tango
{
  class TangoRollingFileAppender : public log4tango::RollingFileAppender
	{
		public:
			/**
			 *
			 **/
			TangoRollingFileAppender (const std::string& name, 
                                const std::string& fileName,
                                size_t maxFileSize);
			/**
			 *
			 **/
			virtual ~TangoRollingFileAppender ();

      /**
			 *
			 **/
      virtual bool isValid (void) const;
	};

} // namespace tango

#endif // _LOG4TANGO_TANGO_ROLLING_FILE_APPENDER_H_

#endif // TANGO_HAS_LOG4TANGO
