/*
 * coutappender.h
 *
 * by NL - SOLEIL - 01/2003.
 *
 * $Revision: 3.0 $
 *
 * $Log: coutappender.h,v $
 * Revision 3.0  2003/03/25 16:41:59  taurel
 * Many changes for Tango release 3.0 including
 * - Added full logging features
 * - Added asynchronous calls
 * - Host name of clients now stored in black-box
 * - Three serialization model in DS
 * - Fix miscellaneous bugs
 * - Ported to gcc 3.2
 * - Added ApiUtil::cleanup() and destructor methods
 * - Some internal cleanups
 * - Change the way how TangoMonitor class is implemented. It's a recursive
 *   mutex
 *
 * Revision 2.2  2003/03/11 17:55:49  nleclercq
 * Switch from log4cpp to log4tango
 *
 * Revision 2.1  2003/02/17 14:57:39  taurel
 * Added the new Tango logging stuff (Thanks Nicolas from Soleil)
 *
 */

#ifndef _COUT_APPENDER_H_
#define _COUT_APPENDER_H_

#if defined(TANGO_HAS_LOG4TANGO)

namespace Tango {

class CoutAppender : public log4tango::LayoutAppender
{
public:
  /**
   *
   **/
  CoutAppender (const std::string& name);

  /**
   *
   **/
  virtual ~CoutAppender ();

  /**
   *
   **/
  inline virtual bool reopen() {
    return true;
  }

  /**
   *
   **/
  inline virtual void close() {
    //no-op
  }

protected:
  /**
   *
   **/
  virtual int _append (const log4tango::LoggingEvent& event); 
};

} // namespace tango

#endif // _COUT_APPENDER_H_

#endif // TANGO_HAS_LOG4TANGO
