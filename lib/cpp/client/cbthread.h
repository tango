//=============================================================================
//
// file :               cbthread.h
//
// description :        Include for the CallBackThread object.
//			This class implements the callback thread
//
// project :            TANGO
//
// author(s) :          E.Taurel
//
// $Revision: 3.0 $
//
// $Log: cbthread.h,v $
// Revision 3.0  2003/03/25 16:30:45  taurel
// Change revision number to 3.0 before release 3.0.0 of Tango lib
//
// Revision 1.1  2003/03/20 08:56:12  taurel
// New file to support asynchronous calls
//
//
//
// copyleft :           European Synchrotron Radiation Facility
//                      BP 220, Grenoble 38043
//                      FRANCE
//
//=============================================================================

#ifndef _CBTHREAD_H
#define _CBTHREAD_H

#include <tango.h>


namespace Tango
{

class CbThreadCmd: public omni_mutex
{
public:
	CbThreadCmd():stop(false) {};
	void stop_thread() {omni_mutex_lock(*this);stop=true;}
	void start_thread() {omni_mutex_lock(*this);stop=false;}
	bool is_stopped() {omni_mutex_lock(*this);return stop;}
	
	bool stop;
};

//=============================================================================
//
//			The CallBackThread class
//
// description :	Class to store all the necessary information for the
//			polling thread. It's run() method is the thread code
//
//=============================================================================


class CallBackThread: public omni_thread
{
public:
	CallBackThread(CbThreadCmd &cmd,AsynReq *as):shared_cmd(cmd),
						     asyn_ptr(as) {};
	
	void *run_undetached(void *);
	void start() {start_undetached();}
	
	CbThreadCmd	&shared_cmd;
	AsynReq		*asyn_ptr;
};



} // End of Tango namespace

#endif /* _CBTHREAD_ */
