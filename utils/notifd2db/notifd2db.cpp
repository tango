 static const char *RcsId = "$Id: notifd2db.cpp,v 1.6 2008/04/24 12:06:27 tiagocoutinho Exp $";

/*
 * this program will export the Notification Factory to the TANGO database
 *
 * - andy 30nov03
 *		
 *  $Revision: 1.6 $
 *
 *  $Log: notifd2db.cpp,v $
 *  Revision 1.6  2008/04/24 12:06:27  tiagocoutinho
 *  fix bug 1950478: notifd2db hangs
 *
 *  Revision 1.5  2008/03/03 10:22:15  taurel
 *  - Added retries around DB object creation and calls (in case of heavily loaded DB)
 *
 *  Revision 1.4  2005/05/11 12:28:42  taurel
 *  - Now the FQDN is stored in db instead of just the host name
 *  - Added one platform (don't remember which one exactly) in Makefile
 *
 *  Revision 1.3  2005/03/14 09:33:56  taurel
 *  - Added the -o option to store notifd IOR in DS file for device server started with
 *    the -file option
 *
 *  Revision 1.2  2004/04/20 14:08:07  taurel
 *  - Fix bug if host name is returned with DNS info. In this case,
 *    remove DNS info.
 *
 *  Revision 1.1.1.1  2004/03/03 13:04:33  taurel
 *  The notifd2db command project
 *
 */

#include <tango.h>
#include <iostream>
#include <fstream>

#ifdef WIN32
#include <process.h>
#else
#include <netdb.h>
#endif

#define DEFAULT_FACT_IOR_FILE	"/tmp/rdifact.ior"
#define BASE_LINE				"notifd"
#define	END_NOTIFD_LINE			"/DEVICE/notifd:"

void check_args(int,char *[],vector<string> &,string &,string &);
void print_usage();
void append_to_file(string &,string &);


int main(unsigned int argc, char **argv)
{
	bool def = false;
	vector<string> file_names;
	string file;
	string host_name;
	
	check_args(argc,argv,file_names,file,host_name);
	
	ifstream ior_file;
	if (file.size() == 0)
	{
		ior_file.open(DEFAULT_FACT_IOR_FILE);
		def = true;
	}
	else
	{
		ior_file.open(file.c_str());
		if (!ior_file)
		{
			if (def == true)
				cout << "cannot open default file (/tmp/rdifact.ior). Please check it exists !\n";
			else
				cout << "cannot open file " << file << " please check if it exists !\n";
			exit(-1);
		}
	}
	


	string ior_string;
   	ior_file >> ior_string;

//	cout << ior_string << endl;

	unsigned long nb_files = file_names.size();
	if (nb_files == 0)
	{
        cout << "going to export notification service event factory to Tango database ...\n";
		
		Tango::Database *tango_db;

		int num_retries = 3;
		bool failed = false;

		while (num_retries > 0)
		{
			try
			{
				tango_db = new Tango::Database();
				tango_db->set_timeout_millis(10000);
				num_retries = 0;
			}
			catch (Tango::DevFailed &e)
			{
				num_retries--;
				if (num_retries == 0)
				{
					cout << "Can't create Tango database object" << endl;
					Tango::Except::print_exception(e);
					return(-1);
				}
				cout << "Can't create Tango database object, retrying...." << endl;
			}
		}
		
		string notifd_factory_name;
		char buffer[256];
		Tango::DevVarStringArray *dev_export_list = new Tango::DevVarStringArray;
		CORBA::Any send;

		dev_export_list->length(5);
		if (host_name.size() == 0)
		{
			gethostname(buffer,256);
			host_name = buffer;

			string::size_type pos = host_name.find('.');
			if (pos == string::npos)
			{
				struct hostent *he;
				he = gethostbyname(buffer);

				if (he != NULL)
				{
					string na(he->h_name);
//					cout << "Name returned by gethostbyname = " << na << endl;
					pos = na.find('.');
					if (pos == string::npos)
					{
						char **p;
						for (p = he->h_aliases;*p != 0;++p)
						{
							string al(*p);
							pos = al.find('.');
							if (pos != string::npos)
							{
//								cout << "FQDN = " << al << endl;
								host_name = al;
								break;
							}
					
						}
					}
					else
						host_name = na;
				}
				else
				{
					cout << "gethostbyname failed..." << endl;
					return(-1);
				}
			}

		}
	
		notifd_factory_name = "notifd/factory/"+host_name;
		(*dev_export_list)[0] = CORBA::string_dup(notifd_factory_name.c_str());
		(*dev_export_list)[1] = CORBA::string_dup(ior_string.c_str());
		(*dev_export_list)[2] = CORBA::string_dup(host_name.c_str());
#ifdef STRSTREAM
		ostrstream ostream;
#else
		ostringstream ostream;
#endif /* STRSTREAM */
#ifndef WIN32
		ostream << getpid() << ends;
#else
		ostream << _getpid() << ends;
#endif /* WIN32 */
#ifdef STRSTREAM
		(*dev_export_list)[3] = CORBA::string_dup(ostream.str());
		delete [] (ostream.str());
#else
		(*dev_export_list)[3] = CORBA::string_dup(ostream.str().c_str());
#endif /* STRSTREAM */
		(*dev_export_list)[4] = CORBA::string_dup("1");
		send <<= dev_export_list;

//
// Call db server with retries in case of Timeout (massive crates re-start after power cut)
//

		num_retries = 3;
		failed = false;

		while (num_retries > 0)
		{
			try
			{
				CORBA::Any_var received = tango_db->command_inout("DbExportEvent",send);
				cout << "Successfully  exported notification service event factory for host " << host_name << " to Tango database !\n";
				num_retries = 0;
			}
			catch (Tango::CommunicationFailed &e)
			{
				if (e.errors.length() >= 2)
				{
					if (::strcmp(e.errors[1].reason.in(),"API_DeviceTimedOut") == 0)
					{
						if (num_retries != 0)
						{
							num_retries--;
							if (num_retries == 0)
							{
								failed = true;
							}
						}
					}
					else
					{
						failed = true;
						num_retries = 0;
					}
				}
				else
				{	
					failed = true;
					num_retries = 0;
				}
			}
			catch (...)
			{
				failed = true;
				num_retries = 0;
			}
		}

		if (failed == true)
		{
			cout << "Failed to export notification service event factory to TANGO database\n";
			return(-1);
		}			
	}
	else
	{
		for (unsigned loop = 0;loop < nb_files;loop++)
			append_to_file(file_names[loop],ior_string);
			
		cout << "Successfully exported notification service event factory to device server file(s) !\n";
	}
	
	
	return(0);
}



void check_args(int argc,char *argv[],
		vector<string> &file_names,
		string &file,string &host)
{
	int ind = 1;

	while (ind < argc)
	{
		if (argv[ind][0] == '-')
		{
			switch (argv[ind][1])
			{

			case 'h':
				print_usage();
				break;
				
			case 'o':
				if (strlen(argv[ind]) != 2)
				{
					print_usage();
				}
				else
				{
					if (ind == (argc - 1))
					{
						print_usage();
					}
					ind++;
					file_names.push_back(argv[ind]);
					ind++;
				}
				break;
				
			default :
				print_usage();
				break;
			}
		}
		else
		{
			if ((file.empty() == true) && (ind == 1))
				file = argv[ind];
			else
			{
				if (ind == 2)
					host = argv[ind];
				else
				{
					print_usage();
				}
			}
			ind++;
		}
	}
	
/*	unsigned long l;
	for (l = 0;l < file_names.size();l++)
		cout << "DS File name = " << file_names[l] << endl;
	if (file.empty() == false)
		cout << "Notifd file = " << file << endl;
	if (host.empty() == false)
		cout << "Notifd host = " << host << endl;*/
}

void print_usage()
{
	cerr << "notifd usage: notifd [notifd_ior_file] [host] [-h] [-o DS file]\nDefault notifd_ior_file=/tmp/rdifact.ior" << endl;
	exit(-1);
}


void append_to_file(string &file_name,string &ior_string)
{
        cout << "going to export notification service event factory to device server property file(s) ...\n";

	fstream ds_file;
	
	ds_file.open(file_name.c_str(),ios::in);
	
	if (!ds_file)
	{
		cout << "Can't open file " << file_name << endl;
		exit(-1);
	}
	
	vector<string> file_lines;

//
// Read all file
//

	char tmp[4096];
	while(ds_file.getline(tmp,4096))
	{
		string tmp_str(tmp);
		file_lines.push_back(tmp);
	}
	
//
// Close file
//

	ds_file.close();
	
	unsigned long nb_lines = file_lines.size();
	unsigned long loop;
	bool found = false;

//
// Find device server instance name
//

	string instance_name;
	for (loop = 0;loop < nb_lines;loop++)
	{
		string line(file_lines[loop]);
		if (line[0] == '#')
			continue;
		string::size_type pos;
		if ((pos = line.find(':')) != string::npos)
		{
			string bef;
			bef = line.substr(0,pos);
			if ((pos = bef.find("DEVICE")) != string::npos)
			{
				string::size_type first,second;
				if ((first = bef.find('/')) == string::npos)
				{
					cerr <<  "Wrong syntax in file " << file_name << " at line " << loop << endl;
					exit(-1);
				}
				first++;
				if ((second = bef.find('/',first)) == string::npos)
				{
					cerr <<  "Wrong syntax in file " << file_name << " at line " << loop << endl;
					exit(-1);
				}
				instance_name = bef.substr(first,second - first);
				break;
			}
		}
	}
	
	if (instance_name.empty() == true)
	{
		cerr << "Can't find device server instance in file " << file_name << "!!!" << endl;
		exit(-1);
	}
//	cout << "Instance name = " << instance_name << endl;
	
		
	for (loop = 0;loop < nb_lines;loop++)
	{
		string line(file_lines[loop]);
		if (line.find(BASE_LINE) != string::npos)
		{
		
//
// Remove all spaces or tab characters
//

			string::size_type pos,idx;
			idx = 0;
			while((pos = line.find(" ",idx)) != string::npos)
			{
				line.erase(pos,1);
				idx = pos;
			}
			idx = 0;
			while((pos = line.find('\t',idx)) != string::npos)
			{
				line.erase(pos,1);
				idx = pos;
			}
		
			
//
// Replace IOR
//

			pos = line.find("/DEVICE/notifd:");
			if (pos == string::npos)
			{
				cerr << "Wrong syntax in file " << file_name << ", line " << loop+1 << endl;
				exit(-1);
			}
			
			string::iterator ite = line.begin();
			ite = ite + pos + 15;
			ior_string = ior_string + '"';
			ior_string.insert(0," \"");
			line.replace(ite,line.end(),ior_string);
			file_lines[loop] = line;
			found = true;
			break;
		}
	}

//
// If IOR does not exist, add it
//

	if (found == false)
	{
		string new_line(BASE_LINE);
		new_line = new_line + '/' + instance_name + END_NOTIFD_LINE + ' ' + '"' + ior_string + '"';
		file_lines.push_back(new_line);
	}


	nb_lines = file_lines.size();

//
// Re-write file
//

	ofstream new_ds_file;
	new_ds_file.open(file_name.c_str(),ios::out|ios::trunc);
	if (!new_ds_file)
	{
		cerr << "Can't re-open file " << file_name << " for writing" << endl;
		exit(-1);
	}
	
	nb_lines = file_lines.size();
		
	for (loop = 0;loop < nb_lines;loop++)
	{
		new_ds_file.write(file_lines[loop].c_str(),file_lines[loop].size());
		new_ds_file.put('\n');
	}
		
	ds_file.close();
	
}
